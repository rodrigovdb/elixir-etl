# Parser

Extract, Transform and Load.

## Running

```
$ mix parse
```
For more details, pĺease check `lib/mix/tasks/parse.ex`

## Testing

```
$ mix test
```
For more details, please check `test/transformer_test.exs` and `test/loader_test.exs`

## Debuging

```
$ iex -S mix
iex(1)> api_url = "https://delivery-center-recruitment-ap.herokuapp.com"
iex(2)> payload = Extractor.from_file |> Transformer.parse
iex(3)> HTTPoison.post(api_url, payload, request_headers)
```
