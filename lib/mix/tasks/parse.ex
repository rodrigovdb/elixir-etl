defmodule Mix.Tasks.Parse do
  use Mix.Task

  @moduledoc """
  Mix task to run complete ETL
  """

  @shortdoc "Run project ETL"
  def run(_) do
    Extractor.from_file
      |> Transformer.parse
      |> Loader.deliver
      |> IO.puts
  end
end
