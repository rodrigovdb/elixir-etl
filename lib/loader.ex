defmodule Loader do
  @moduledoc """
  Load data to owned API
  """

  @url "https://delivery-center-recruitment-ap.herokuapp.com"

  def deliver(payload) do

    HTTPoison.start
    case HTTPoison.post(@url, payload, request_headers()) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        body
      {:ok, %HTTPoison.Response{status_code: 404}} ->
        "Not found"
      {:ok, %HTTPoison.Response{status_code: 400, body: body}} ->
        "Error: #{body}"
      {:error, %HTTPoison.Error{reason: reason}} ->
        reason
      _ ->
        "Unknown error"
    end
  end

  def request_headers do
    now = DateTime.utc_now |> DateTime.to_string
    [
      {"content-type", "application/json"},
      {"X-Sent", now}
    ]
  end
end
