defmodule Extractor do
  @moduledoc """
  Extract data from a source. For now, from a sample JSON file.
  """

  @doc """
  Read data from sample file.
  """
  def from_file do
    {:ok, content} = Path.join([File.cwd!, "test", "samples", "payload.json"]) |> File.read

    content
  end

  def expectation do
    {:ok, content} = Path.join([File.cwd!, "test", "samples", "expectation.json"]) |> File.read

    content
  end
end
