defmodule Transformer do
  @moduledoc """
  Parse raw content into something usefull.
  """

  def parse(raw) do
    Jason.decode!(raw)
      |> assemble_response
      |> Jason.encode!
  end

  def assemble_response(content) do
    %{
      "externalCode"  => "#{content["id"]}",
      "storeId"       => content["store_id"],
    } |> Map.merge(money_data(content))
      |> Map.merge(shipping_data(content["shipping"]))
      |> Map.merge(%{"customer" => build_customer(content["buyer"])})
      |> Map.merge(%{"items"    => build_items(content["order_items"])})
      |> Map.merge(%{"payments" => build_payments(content["payments"])})
  end

  def money_format(number) do
    "#{:io_lib.format("~.2f", [number])}"
  end

  def state_from_name(name) do
    %{
      "São Paulo"         => "SP",
      "Paraná"            => "PR",
      "Santa Catarina"    => "SC",
      "Rio Grande do Sul" => "RS",
    }[name]
  end

  def datetime_format(orig) do
    {_ok, utc} = orig |> Calendar.DateTime.Parse.rfc3339_utc

    utc |> Calendar.DateTime.Format.rfc3339
  end

  def money_data(content) do
    %{
      # Money, needs to add decimal precision
      "subTotal"        => money_format(content["total_amount"]),
      "deliveryFee"     => money_format(content["total_shipping"]),
      "total_shipping"  => money_format(content["total_shipping"]),
      "total"           => money_format(content["paid_amount"]),
    }
  end

  def shipping_data(shipping) do
    %{
      # Data from shipping
      "country"       => shipping["receiver_address"]["country"]["id"],
      "state"         => state_from_name(shipping["receiver_address"]["state"]["name"]),
      "city"          => shipping["receiver_address"]["city"]["name"],
      "district"      => shipping["receiver_address"]["neighborhood"]["name"],
      "street"        => shipping["receiver_address"]["street_name"],
      "complement"    => shipping["receiver_address"]["comment"], # is it comment?
      "latitude"      => shipping["receiver_address"]["latitude"],
      "longitude"     => shipping["receiver_address"]["longitude"],
      "dtOrderCreate" => datetime_format(shipping["date_created"]), # TODO: Fix with timezone
      "postalCode"    => shipping["receiver_address"]["zip_code"],
      "number"        => "0", # no idea about this one
    }
  end

  def build_customer(buyer) do
    %{
        "externalCode"  => "#{buyer["id"]}",
        "name"          => buyer["nickname"],
        "email"         => buyer["email"],
        "contact"       => "#{buyer["phone"]["area_code"]}#{buyer["phone"]["number"]}"
      }
  end

  ##
  ## Build order items list
  ##
  def build_items([]) do
    []
  end

  def build_items([first_order | orders]) do
    build_items(first_order) ++ build_items(orders)
  end

  def build_items(order) do
    [
      %{
        "externalCode"  => order["item"]["id"],
        "name"          => order["item"]["title"],
        "price"         => order["unit_price"],
        "quantity"      => order["quantity"],
        "total"         => order["full_unit_price"],
        "subItems"      => []
      }
    ]
  end

  ##
  ## Build payments data
  ##
  def build_payments([]) do
    []
  end

  def build_payments([first | payments]) do
    build_payments(first) ++ build_payments(payments)
  end

  def build_payments(payment) do
    [
      %{
          "type"  => payment["payment_type"] |> String.upcase,
          "value" => payment["total_paid_amount"]
      }
    ]
  end
end
