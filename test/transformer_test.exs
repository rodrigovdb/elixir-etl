defmodule TransformerTest do
  use ExUnit.Case
  doctest Transformer

  test "match with final expectation" do
    content     = Extractor.from_file
    expectation = Extractor.expectation

    assert Transformer.parse(content) == Jason.encode!(Jason.decode!(expectation))
  end
end
