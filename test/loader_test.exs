defmodule LoaderTest do
  use ExUnit.Case
  doctest Loader

  test "handle 200 response" do
    payload = Extractor.from_file |> Transformer.parse

    assert Loader.deliver(payload) == "OK"
  end

  test "handle 400 response" do
    payload = Extractor.from_file |> Transformer.parse |> String.replace("total_shipping", "something_else")

    assert Loader.deliver(payload) == "Error: [total_shipping] are required"
  end

  test "handler error response" do
    assert Loader.deliver("foo") == "Unknown error"
  end
end
