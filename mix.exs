defmodule Parser.MixProject do
  use Mix.Project

  def project do
    [
      app: :parser,
      version: "0.1.0",
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      applications: [:httpoison],
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
      {:calendar, "~> 1.0.0"},

      # Coding Style
      {:credo, "~> 1.1.0", only: [:dev, :test], runtime: false},

      # HTTP Client
      {:httpoison, "~> 1.6"},

      # Test Mocks
      {:mock, "~> 0.3.0", only: :test}
    ]
  end
end
